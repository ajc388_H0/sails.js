//Imports
var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt');

//Serialization
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findOne({ id: id } , function (err, user) {
    done(err, user);
  });
});

passport.use(new localStrategy({
  usernameField: 'email',
  passwordField: 'password'
  },

  function(email, password, done) {

    User.findOne({ email: email}, function (err, user) {
      if (err) return done(err);
      if (!user) return done(null, false, { message: 'Incorrect email or password.'});

      bcrypt.compare(password, user.password, function (err, res) {
        if (!res) return done(null, false, { message: 'Incorrect email or password.'});
        var thisUser = {
          email: user.email,
          createdAt: user.createdAt,
          id: user.id
        };
        return done(null, thisUser, { message: 'Logged in successfully'});
      });
    });
  }
));
